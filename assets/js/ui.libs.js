'use strict';

// 유틸들
window.DisplayUtils = {
  /**
   * 엘리먼트의 클래스 토글
   * @author Natasha
   * @param {html DOM} target 대상 엘리먼트
   * @param {String} str 특정 클래스명을 토글하려면 전달 (default 'on')
   * @returns target
   */
  toggleOn(target, str='on') {
    const flag = $(target).hasClass(str);
    // console.log(`toggle class : ${flag}`);
    $(target).toggleClass(str, !flag);
    return flag;
  },
};

// UI 관리용 객체들
(function($) {
  /**
   * Collapsable 관리
   * 형제들과 상호작용을 하지 않고 독립적으로 펼치거나 접히는 UI
   * @author Natasha
   * @usage $('.ui-collapse').collapsable();
   */
  class Collapsable {
    constructor(element, speed=200) {
      this.element = element;
      this.$btn = $(element).children('.ui-collapse-text');
      this.$collapsable = $(element).children('.ui-collapsable');
      this.speed = speed;
		  // console.log("%cnew Collapsable --------------------", "color:purple;font-size: 14px;");
		  // console.log(this.element);
    }
    setup() {
      this.$btn.on('click', $.proxy(this.collapse, this));
    }
    open() {
        // console.log('collapse open');
        if(!$(this.element).hasClass("on")) {
          $(this.element).addClass('on');
        }
        this.$collapsable.slideDown(this.speed);
    }
    close() {
        // console.log('collapse close');
        if($(this.element).hasClass("on")) {
          $(this.element).removeClass('on');
        }
        this.$collapsable.slideUp(this.speed);
    }
    collapse(e) {
      e.stopPropagation();
      DisplayUtils.toggleOn(this.element) ? this.close() : this.open();
    }
  }

  Collapsable.NAME = 'Collapsable';
  Collapsable.CORE = {
    createInstance(element, option) {
      let instance = $(element).data(Collapsable.NAME);
      // console.log(instance);
      if(!instance) {
        instance = new Collapsable(element);
        instance.setup();
        instance.close(); //일단 접힘 상태에서 시작함
        $(element).data(Collapsable.NAME, instance);
      }

      // console.log(`option is ${option}`);
      if(typeof option === 'string') {
        instance[option]();
      }
      return instance;
    }
  };
  // Collapseable

  $.fn.collapsable = function(option) {
    $(this).each((i, el) => Collapsable.CORE.createInstance(el, option));
    return this;
  };

}(jQuery));
