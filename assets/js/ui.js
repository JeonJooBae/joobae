/**
 * ready
 * @requires /assets/js/ui.libs.js
 */
$(function() {
  // 토글 UI
  $('.ui-toggle').on('click', function() {
    DisplayUtils.toggleOn(this);
  });

  // 접히는 UI
  $('.ui-collapse')
    .collapsable()
    .filter('.collapse-open')
    .collapsable('open');

  // tree UI
  $('.js-tree--nav .js-tree')
    .jstree()
    .bind("select_node.jstree", function(e, data) {
      window.location.href = data.node.a_attr.href;
    });
  $('.js-tree--checkbox .js-tree').jstree({
    "checkbox" : {
      "keep_selected_style" : false
    },
    "plugins" : [ "wholerow", "checkbox" ]
  });
  $('.btn-tree.open').on('click', function(){
    const $tree = $(this).closest('[class*="js-tree--"]').find('.js-tree');
    $tree.jstree('open_all');
  });
  $('.btn-tree.close').on('click', function(){
    const $tree = $(this).closest('[class*="js-tree--"]').find('.js-tree');
    $tree.jstree('close_all');
  });
  //--- tree UI


  /* Modal */
  $(".modal").each(function(index) {
    if ($(this).length > 0){
      /**
       * Options
       * @author Boris
       */
      let modal = $(this).data('modal'),
          modalOpen = $(this).data('open'),
          modalDimmed = $(this).data('dimmed'),
          modalClass = $(this).data('class'),
          modalWidth = $(this).data('width'),
          modalEffect = $(this).data('effect'),
          position = $(this).data('position'),
          resizable = $(this).data('resize'),
          draggable = $(this).data('drag');

      // selector
      const modalPlugin = $('.'+modal);
      
      modalPlugin.dialog({
        autoOpen: modalOpen ? modalOpen : false,
        modal: (modalDimmed !== undefined) ? modalDimmed : true,
        show: modalEffect ? modalEffect : 'fade',
        resizable: resizable ? resizable : false,
        draggable: draggable ? draggable : false,
        width: modalWidth,
        classes: {
          "ui-dialog": modalClass
        }
      });
      // resize
      $(window).resize(function(){
        if (position){
          modalPlugin.dialog("option", "position", { my: "center", at: "center", of: window });
        }
      });
      
      // close
      modalPlugin.find(".page-btn .btn").on("click", function() {
        modalPlugin.dialog("close");
      });
    }
  });
  // opener
  $('.opener').each(function() {
    let opener = $(this).data('opener');
    $('.'+opener).on("click", function() {
      let remodal = opener.replace('opener','modal');
      $('.'+remodal).dialog("open");
    });
  })


  /* Tabs */
  $(".tabs").each(function() {
    /**
     * Options
     * @author Boris
     */
    let active = $(this).data('active');
    
    $(this).tabs({
      active: active ? active : 0
    });
  })
});
