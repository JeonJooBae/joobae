var suPubList = null;
function pubGuideTableInit(objOptions) {
  if(suPubList != null) {
    suPubList.destroy();
  }
  let subPubDataNum = 0;
  let ajaxURL = objOptions.pubUrl;
  suPubList = $('#su-pub-list').DataTable( {
    "searching": false,
    "ordering":  true,
    "paging": false,
    "ajax": ajaxURL,
    "columns": [
        { "data": "번호", "className":"pub-no",
          render: function(data, type) {
            subPubDataNum++;
            return subPubDataNum;
          }
        },
        { "data": "화면ID", "className":"pub-id" },
        { "data": "D1", "className":"pub-d1" },
        { "data": "D2", "className":"pub-d2" },
        { "data": "D3", "className":"pub-d3" },
        { "data": "D4", "className":"pub-d4" },
        { "data": "D5", "className":"pub-d5" },
        { "data": "구분1", "className":"pub-part1" }, //사용자(USR), 어드민(ADM)
        { "data": "구분2", "className":"pub-part2" }, //페이지(PG), 탭(TAB), 팝업(POP), 레이어 팝업(LY), 
        { "data": "요청", "className":"pub-req" }, //ASAP(MM-DD)
        { "data": "PATH", "className":"pub-path",
          render: function(data, type, row) {
            if(data == '' || data == null  || data == ' ') {
              let part1 = row["구분1"],
                  myId = row['화면ID'],
                  prePath = '';
              if(myId == '') { 
                return '';
              }
              if(part1 == '' || part1 == 'USR') {
                prePath = '/pages/user/';
              } else if(part1 == 'ADM') {
                prePath = '/pages/admin/';
              }
              return "<a href='" + prePath + myId + ".html' target='_blank'>" + prePath + myId + ".html</a>";
            } else {
              return '<a href="' + data + '" target="_blank">' + data + '</a>';
            }
          }}, //URL
        { "data": "Figma", "className":"pub-figma",
          render: function(data, type) {
            if(data == '' || data == null) {
              return '';
            } else {
              return "<a href='" + data + "' target='_blank'>VIEW</a>";
            }
          } },
        { "data": "착수일", "className":"pub-start" },
        { "data": "완료일", "className":"pub-end" },
        { "data": "담당자", "className":"pub-worker" },
        { "data": "진행상태", "className":"pub-status" }, //진행전, 진행중, 검수요청, 퍼블완료
        { "data": "비고", "className":"pub-etc" }
    ]
  } );
}
function pubListMenuEventHandler(el) {
  let element = $(el);
  let eventTargetEl = element.find('a');
  let frameId = '#pub-guide-frame';

  eventTargetEl.off('click');
  eventTargetEl.on('click', function(e) {
    let self = $(this),
    selfVal = self.attr('data-url');
    $(frameId).off('load');
    $(frameId).on('load',function (e) {
        e.preventDefault(); 
        if($(frameId)[0].contentWindow.pubGuideTableInit) $(frameId)[0].contentWindow.pubGuideTableInit({pubUrl:'/guide/data/' + selfVal})
    });
  });
  eventTargetEl.eq(0).trigger('click');
}
function pubGnbInit(el){
  let element = $(el);
  let eventListEl = $(el + ' > li');
  let evenLinkEl = eventListEl.children('a'); 
  let submenuEl = eventListEl.children('div'); 
  evenLinkEl.on('mouseover foucs', function(e) {
    e.preventDefault();
    let self = $(this);
    submenuEl.hide();
    self.next('div').show();
  });
  eventListEl.find('ul > li > a').on('click', function(e) {
    let self = $(this);
    if(self.attr('href') == '#') {
      e.preventDefault();
      alert('준비중입니다!');
    }
  })
}
$(document).ready(function() {
  pubGnbInit('.pub-gnb');
} );